## Django Generic Polls API

Create the environment and install requirements

```
virtualenv --python=python3 venv

source ./venv/bin/activate

pip3 install src/requirements.txt
```

Nginx configuration: 

```
  location /polls/ {
    include uwsgi_params;
    uwsgi_pass unix:///tmp/polls_app.socket;
  }

```

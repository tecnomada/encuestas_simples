from django.urls import path
import polls.api as api

urlpatterns = [
    path('create_session/', api.get_session),
    path('<str:pollid>/', api.get_poll),
    
]

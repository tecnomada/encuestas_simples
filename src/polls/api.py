from django.http import JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache

import logging
import time
import json
import datetime
from .models import Nonce, MaxInt, Vote, Session
import random


# Get instance of logger
logger = logging.getLogger("loggerdebug")

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


@csrf_exempt
def get_poll(request, pollid):
    """ Handle polls interactions.
    
    When GET, return session vote, poll summary and generate a new nonce.
    
    When POST, check if nonce and browsing data matches to an existing and valid nonce, if it does record vote. 
    
    """
    # We need a session
    if "HTTP_X_SESSIONID" not in request.META:
        return HttpResponse("400 - Bad request", status=400)
    
    sessid = request.META["HTTP_X_SESSIONID"]
    # Attempt to get the session data
    session = Session.objects.get(sesshash = int(sessid))
        
    if request.method == "POST":
        # Get nonce data
        noncehash = request.META["HTTP_X_NONCE"]
        nonceresp = Nonce.objects.filter(
              useragent = request.META['HTTP_USER_AGENT'],
              ip        = get_client_ip(request),
              sessid    = session.id,
              pollid    = pollid,
              noncehash = int(noncehash))
        
        nonceobj = nonceresp[0]
        
        if (datetime.datetime.now() - nonceobj.created_at.replace(tzinfo=None)).seconds < 3600*2:
            votevalue = request.POST.get("votevalue")
            
            # Get vote value
            votequery  = Vote.objects.filter(sessid = session.id, pollid = pollid)
            if len(votequery) > 0: 
                voteresp   = votequery[0] 
                voteresp.updates += 1
                voteresp.votevalue = votevalue
                voteresp.save()
            else: 
                newvote = Vote(
                    votevalue = votevalue[0:20],
                    sessid    = session.id,
                    pollid    = pollid,
                    updates   = 1
                )
                newvote.save()
        return JsonResponse({})
    elif request.method == "GET":
        # Create nonce
        noncehash = random.randint(MaxInt / 128, MaxInt)
        nonceobj = \
          Nonce(
            useragent = request.META['HTTP_USER_AGENT'],
            ip        = get_client_ip(request),
            sessid    = session.id,
            pollid    = pollid,
            noncehash = noncehash
          )
        nonceobj.save()
        
        # Handle cached poll summary
        # Update every 5 seconds
        timestamp = cache.get("pollsummaryTS_" + pollid)
        pollsumm  = cache.get("pollsummary_" + pollid)
        data      = None
        if timestamp is not None and pollsumm is not None:
            try:
                if time.time() - float(timestamp) < 5:
                    data = json.loads(pollsumm)
            except: 
                pass

        # If no data has allready been set, get poll summary from database and save it to cache:
        if data is None:
            cursor = Vote.objects.raw('SELECT 1 as id, votevalue , COUNT(id) as n FROM polls_vote WHERE pollid = %s GROUP BY votevalue', [pollid])
            data = {x.votevalue: x.n for x in cursor}
            # store in cache
            cache.set("pollsummaryTS_" + pollid, str(time.time()))
            cache.set("pollsummary_" + pollid,   json.dumps(data))
            
        try:
            votequery  = Vote.objects.filter(sessid = session.id, pollid = pollid)
            voteresp   = votequery[0] if len(votequery) > 0 else None
        except:
            voteresp  = None
        
        data = {"nonce": noncehash, "data": data}
        data["vote"] = voteresp.votevalue if voteresp is not None else None
        data["votetime"] = voteresp.updated_at if voteresp is not None else None
        
        return JsonResponse(data)
      
      
def get_session(request):
    """ Create a new session
    """
    sessobj = \
      Session(
        sesshash = random.randint(MaxInt / 128, MaxInt)
      )
    sessobj.save()
    
    return JsonResponse({ "sessionid": sessobj.sesshash  })

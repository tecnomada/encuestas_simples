from django.db import models
import hashlib
"""
CREATE TABLE PSessions (
  sguid INT UNSIGNED NOT NULL AUTO_INCREMENT,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE PNonces (
  nguid INT UNSIGNED NOT NULL AUTO_INCREMENT,
  created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  UserAgent VARCHAR(100) DEFAULT "",
  IP VARCHAR(30),
  sguid INT UNSIGNED, 
  pguid INT UNSIGNED,
  nonce INT UNSIGNED
);

CREATE TABLE PVotes (
  pguid VARCHAR(10),
  sguid INT UNSIGNED,
  nonce INT UNSIGNED,
  vote VARCHAR(10),
);
"""

MaxInt = 2**45

class Session(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    sesshash   = models.IntegerField(db_index=True, default=0)
    def __str__(self):
        return self.id
      
class Nonce(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    useragent  = models.CharField(max_length=100)
    ip         = models.CharField(max_length=15)
    sessid     = models.IntegerField()
    pollid     = models.CharField(max_length=10, null=False, default="")
    noncehash  = models.IntegerField(db_index=True)
    def __str__(self):
        return self.noncehash      

class Vote(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    updates    = models.IntegerField()
    sessid     = models.IntegerField(db_index=True, default = 0)
    pollid     = models.CharField(max_length=10, null=False, default="")
    votevalue  = models.CharField(max_length=20)
    def __str__(self):
        return self.votevalue
      

